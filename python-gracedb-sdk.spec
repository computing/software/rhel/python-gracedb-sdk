%global srcname gracedb-sdk
%global srcname_underscores %(echo "%{srcname}" | tr - _)
%global srcname_first_letter %(echo "%{srcname}" | cut -c1)

Name:           python-%{srcname}
Version:        0.1.6
Release:        1%{?dist}
Summary:        REST API SDK for GraceDB

License:        GPLv3+
URL:            https://pypi.org/project/%{srcname}/
Source0:        https://files.pythonhosted.org/packages/py3/%{srcname_first_letter}/%{srcname}/%{srcname_underscores}-%{version}-py3-none-any.whl

BuildArch:      noarch

%global _description %{expand:
A modern, performant REST API client for GraceDB, based on Requests.}

%description %_description

%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
Requires:       python%{python3_pkgversion}-requests-gracedb
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pip
BuildRequires:  python%{python3_pkgversion}-wheel
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname} %_description

%prep
%__mkdir_p dist
%__cp %{S:0} dist

%install
%py3_install_wheel %{basename:%{S:0}}

%files -n python%{python3_pkgversion}-%{srcname}
%{python3_sitelib}

%changelog
* Wed Mar 18 2020 Leo Singer <leo.singer@ligo.org> 0.1.6-1
- New upstream release

* Sat Mar 07 2020 Leo Singer <leo.singer@ligo.org> 0.1.5-1
- New upstream release

* Thu Feb 27 2020 Leo Singer <leo.singer@ligo.org> 0.1.4-1
- New upstream release
- License changed to GLPv3+

* Tue Feb 20 2020 Leo Singer <leo.singer@ligo.org> 0.1.2-1
- New upstream release

* Tue Feb 11 2020 Leo Singer <leo.singer@ligo.org> 0.1.1-1
- New upstream release

* Mon Feb 10 2020 Leo Singer <leo.singer@ligo.org> 0.1.0-1
- Initial RPM release
